<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 2018/8/29
 * Time: 11:27
 */

namespace YourApp\Handler;

use GatewayWorker\Lib\Gateway;
use YourApp\util\Common;
use YourApp\util\Jt0001;
use YourApp\util\Jt0100;
use YourApp\util\MsgDecoder;
use YourApp\util\MsgEncoder;

class Jt808Handler
{
    private $clientId;
    private $msg;
    public function __construct($message,$client_id)
    {
        $this->msg=$message;
        $this->clientId=$client_id;
    }
    public function deal(){
        $decoder=new MsgDecoder();
        $encoder=new MsgEncoder();
        // $ret 就是解析后的
        $ret=$decoder->getPackageData($this->msg);
        switch ($ret->MsgHeader->msg_id){
            //通用应答
            case '0001':
                $jt001=new Jt0001($ret->msg_body);
                break;
            case '0002':
                $response_body=Common::ComResponse($ret->MsgHeader->terminal_phone,'0002',0);
                $set=[
                    'phone'=>$ret->MsgHeader->terminal_phone,
                    'msg_type'=>'8001',
                    'flow_id'=>2,
                    'encode_type'=>0
                ];
                $response=$encoder->getResponseString($response_body,$set);
                Gateway::sendToClient($this->clientId,$response);
                break;
            case '0100':
                // 终端注册,回复注册应答
                $jt=new Jt0100($ret->msg_body);
                $body=$jt->response($ret->MsgHeader->flow_id,0,'test_auth_7');
                $set=[
                    'phone'=>$ret->MsgHeader->terminal_phone,
                    'msg_type'=>'8100',
                    'flow_id'=>2,
                    'encode_type'=>0
                ];
                $response=$encoder->getResponseString($body,$set);
                Gateway::sendToClient($this->clientId,$response);
                break;
        }
    }
}