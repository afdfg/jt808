<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 2018/7/7
 * Time: 17:55
 */

namespace YourApp\util;


class Jt0100
{
    public $province_id;
    public $city_id;
    public $client_type;
    public $car_number_color;
    public $car_number;
    public $product_id;
    public $client_id;

    public function __construct($body)
    {
        $this->province_id=substr($body,0,4);
        $this->city_id=substr($body,4,4);

        $this->product_id=pack('H*',substr($body,8,10));

        $this->client_type=substr($body,18,40);

        $this->client_id=pack('H*',substr($body,58,14));

        $this->car_number_color=substr($body,72,2);
        $this->car_number=Common::getString(substr($body,74));//gbk编码
    }
    public function response($flow_id,$result,$auth_string=null){
        $string=pack('n',$flow_id).pack('C',$result);
        if($result==0 && !is_null($auth_string)){
            $string.=Common::setString($auth_string);
        }
        return bin2hex($string);
    }
}
