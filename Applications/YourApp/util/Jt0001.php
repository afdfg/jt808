<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 2018/7/17
 * Time: 14:21
 */

namespace YourApp\util;


use GatewayWorker\Lib\Db;

class Jt0001
{
    protected $flow;
    protected $type_id;
    protected $result;

    public function __construct($body){
        $this->flow=substr($body,0,4);
        $this->type_id=substr($body,4,4);
        $this->result=hexdec(substr($body,8,2));
    }
    public function show(){
        echo "{$this->flow} =====>{$this->type_id} \n result is {$this->result}";
    }
}