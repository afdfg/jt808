<?php

namespace YourApp\util;

/**
 * Class PackageData
 * @package YourApp\util
 * @property MsgHeader $MsgHeader
 * @property String msg_body
 */
class PackageData{

    /**
     * @var MsgHeader 16 bytes的消息头
     */
    protected $MsgHeader;

    /**
     * @var string 消息主体
     */
    protected $msg_body;

    /**
     * @var string 校验码
     */
    protected $check_sum;

    /**
     * @var string 进程id
     */
    protected $channel_id;

    public function __set($name, $value)
    {
        $this->$name=$value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}