<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 2018/7/5
 * Time: 12:10
 */
//
namespace YourApp\util;


/**
 * Class MsgHeader
 * @package YourApp\util
 * @property string $msg_id
 * @property string $terminal_phone
 * @property string $flow_id
 * @property int $msg_body_length
 */
class MsgHeader{

    //消息id byte[0-1]
    protected $msg_id;

    //-----------------  消息体的属性  byte[2-3] start -------------------------//
    //都是10进制
    protected $mgs_body_props_field;

    //消息体长度
    protected $msg_body_length;

    //数据加密方式
    protected $msg_encryption_type;

    //是否分包 true  ====>有消息包封装项
    protected $has_sub_package;

    //保留位
    protected $reserved_bit;
    //-----------------  消息体的属性  byte[2-3]  end -------------------------//


    //终端手机号,直接使用的字符串
    protected $terminal_phone;

    //流水号,10进制
    protected $flow_id;


    //-----------------  消息包封装项  byte[12-15]  satrt -------------------------//
    protected $package_info_filed;

    //信息包数量
    protected $total_sub_package;

    //当前包是第几个 从1开始
    protected $sub_package_seq;


    public function __set($name, $value)
    {
        $this->$name=$value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function isHasSubPackage(){
        return $this->has_sub_package;
    }
}